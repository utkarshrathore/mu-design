import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Buttons from "./components/buttons/Buttons";
import TextField from "./components/textFields/TextField";
import Dropdown from "./components/dropdown/Dropdown";
import Badges from "./components/badges/Badges";
import Tooltip from "./components/tooltip/Tooltip";
import HelloBar from "./components/helloBar/HelloBar";
import ProgressBar from "./components/progressBar/ProgressBar";
import BreadCrumbs from "./components/breadCrumbs/BreadCrumbs";
import Checkbox from "./components/checkboxes/Checkbox";

function App() {
  return (
    <div className="app">
      <div className="container">
        <div className="dFlex">
          <a className="primaryBtn" href="/">home</a>
          <a className="primaryBtn" href="/dropdown">dropdowns</a>
          <a className="primaryBtn" href="/textField">Text Field</a>
          <a className="primaryBtn" href="/buttons">buttons</a>
          <a className="primaryBtn" href="/badges">badges</a>
          <a className="primaryBtn" href="/tooltip">Tooltip</a>
          <a className="primaryBtn" href="/helloBar">Hello Bar</a>
          <a className="primaryBtn" href="/progessBar">Progress Bar</a>
          <a className="primaryBtn" href="/breadCumb">Bread Crumbs</a>
          <a className="primaryBtn" href="/checkboxes">Checkboxes</a>
        </div>
      </div>
      <Router>
        <Routes>
          <Route exact path="/buttons" element={<Buttons />} />
          <Route exact path="/textField" element={<TextField />} />
          <Route exact path="/dropdown" element={<Dropdown />} />
          <Route exact path="/tooltip" element={<Tooltip title={'Title'} children={'hover me to see the tooltip'} tooltipText={'Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.'}/>} />
          <Route exact path="/badges" element={<Badges children={'Rest'} isDisabled={false} img={"../assets/icons/x-close.svg"} />} />
          <Route exact path="/helloBar" element={ <HelloBar children={'Message about the state of this view'} isCenter={true} /> }/>
          <Route exact path="/progessBar" element={<ProgressBar width={'20%'}/>} />
          <Route exact path="/breadCumb" element={<BreadCrumbs/>} />
          <Route exact path="/checkboxes" element={<Checkbox/>} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
