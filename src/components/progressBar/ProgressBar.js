import React from 'react'
import './ProgressBarStyle.css'

const ProgressBar = ({width}) => {
  return (
   <div className="container">
    <div className="dFlex">
        <div className="progessBarOuter">
            <div className="progress"style={{width:width}} ></div>
        </div>
    </div>
   </div>
  )
}

export default ProgressBar