import React, { useState } from "react";
import "./DropdownStyle.css";

const Dropdown = ({ options,type }) => {
  const dropdownType = 'checkbox'
  const [active, setActive] = useState(false);
  const [isError, setIsError] = useState(false);
  const [isDisabled, setIsDisabledr] = useState(false);
  const [selectedOption, setSelectedOption] = useState("Select any Option");

  // const optionsList = options
  const optionsList = [
    "Option 1",
    "Option 2",
    "Option 3",
    "Option 4",
    "Option 5",
    "Option 6",
    "Option 7",
  ];
  return (
    <div className="container">
      <div className="dFlex">
      <div
          className={`dropdownBox ${active ? "active" : ""} ${isError? 'error' : ''} ${isDisabled ? 'disabled':''}`  }
          onBlur={()=>setActive(false)}
          onClick={() => setActive(!active)}
        >
          <label htmlFor="dropdown" className="formLabel">Dropdown Label</label>
          <div className="selectedOption" id="dropdown">
            {selectedOption}{" "}
            <img src="../assets/icons/chevron-down-black.svg" alt="" />{" "}
            <ul className="dropdownList">
            {optionsList.map((option, index) => {
              return (
                dropdownType==='checkbox'? 
                <li
                  className="dropdownListItem"
                  key={index}
                  onClick={() => {
                    setSelectedOption(option);
                  }}
                >
                    <div className="checkboxGrp">
                        <input type="checkbox" id={option}/>
                        <label htmlFor={option}>{option}</label>
                    </div>
                  
                </li>:
                <li
                className="dropdownListItem"
                key={index}
                onClick={() => {
                  setSelectedOption(option);
                }}
              >
                  {option}
                
              </li>
              );
            })}
          </ul>
          </div>
          {
            isError ? <p className="errorMsg">Error message</p>:''
          }
        </div>
      </div>
    </div>
  );
};

export default Dropdown;
