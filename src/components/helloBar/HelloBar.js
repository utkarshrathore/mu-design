import React from "react";
import "./HelloBarStyle.css";
import PrimaryButton from "../buttons/primaryButton/PrimaryButton";

const HelloBar = ({children,isCenter}) => {
  return (
    <div className="container">
      <div className="dFlex">
        <div className={`messageBarOuter ${isCenter?'center':''}`}>
          <div className="messageBox"><img src="../assets/icons/alert-circle.svg" alt="" /> Message about the state of this view</div>
          <div className="btnGrp">
            <PrimaryButton classname={"secondaryBtn"} children={"Cancel"} />
            <PrimaryButton classname={"primaryBtn"} children={"Approve"} />
            {!isCenter?<img src="../assets/icons/x-close.svg" alt="" className="pointer" /> :null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default HelloBar;
