import React from 'react'
import './BadgesStyle.css'

const Badges = ({img,children,isDisabled}) => {
  return (
   <div className="container">
    <div className="dFlex">
    <div className={`badge ${isDisabled ? 'disabled':''}`}>
        <img src={img}/>
        {children}
    </div>
    </div>
   </div>
  )
}

export default Badges