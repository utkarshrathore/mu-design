import React from "react";
import "./BreadCrumbsStyle.css";
const BreadCrumbs = ({}) => {
  const layers = [
    "Layer 1",
    "Layer 2",
    "Layer 3",
    "Layer 4",
    "Layer 5",
    "Layer 6",
  ];
  return (
    <div className="container">
      <div className="dFlex">
        <div className="breadCrumbOuter">
          {layers.map((layer) => {
            return (
              <div className="breadCrumb">
                <p>{layer}</p>
                /
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default BreadCrumbs;
