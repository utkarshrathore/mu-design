import React from 'react'
import './TextFieldStyle.css';

const TextField = () => {
    return (

        <div className="container">

            <br/><br/>

            <h4>Text Fields</h4>

            <div className="formGroup dFlex gap5 flexColumn">
                <label htmlFor="Email">
                    Label
                </label>
                <input
                    className="formInput"
                    type="text"
                    placeholder="Placeholder"
                />
            </div>

            <br/><br/>


            <h4>Text Field Disable</h4>

            <div className="formGroup dFlex gap5 flexColumn">
                <label htmlFor="Email">
                    Label
                </label>
                <input
                    className="formInput"
                    type="text"
                    placeholder="Placeholder"
                    disabled="true"
                />
            </div>

            <br/><br/>


            <h4>Text Field Error</h4>

            <div className="formGroup dFlex gap5 flexColumn">
                <label htmlFor="Email">
                    Label
                </label>
                <input
                    className="formInput error"
                    type="text"
                    placeholder="Placeholder"
                />
                <span className="error-msg">Error message</span>
            </div>

        </div>

    )
}

export default TextField