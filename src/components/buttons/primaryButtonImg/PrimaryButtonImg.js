import React from 'react'

const PrimaryButtonImg = ({onclick,isdisabled,children,img,classname}) => {
  return (
    <button className={classname} onClick={onclick} disabled={isdisabled}><img src={img} alt=""  />{children}</button>
  )
}

export default PrimaryButtonImg