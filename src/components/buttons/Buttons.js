import React from 'react'
import Button from './primaryButton/PrimaryButton'
import './ButtonStyle.css'

import PrimaryBtnDropdown from './primaryBtnDropdown/PrimaryBtnDropdown'
import ButtonDropdownImg from './buttonDropdownImg/ButtonDropdownImg'

const Buttons = () => {
  const log = ()=>{
      console.log('jj');
  }
  return (
   
    <div className="container">
        <h4>Primary</h4>
        <div className="dFlex">
        <Button onclick={log} children={'Rest'} classname={'primaryBtn'} isdisabled={false}/>
        <Button onclick={log} children={'Rest Disabled'} classname={'primaryBtn'} isdisabled={true}/>
        </div>
        <h4 className='mt40'>Primary-icon</h4>
        <div className="dFlex">
          <ButtonDropdownImg img={'../assets/icons/faceIcon.svg'} children={'Rest'} classname={'primaryBtn'} isdisabled={false} />
          <ButtonDropdownImg img={'../assets/icons/faceIcon.svg'} children={'Rest Disabled'} classname={'primaryBtn'} isdisabled={true} />
        </div>
        <h4 className='mt40'>Primary Dropdown</h4>
        <div className="dFlex">
          <PrimaryBtnDropdown onclick={log} children={'Rest'} classname={'primaryBtn'} isdisabled={false}/>        
          <PrimaryBtnDropdown onclick={log} children={'Rest Disabled'} classname={'primaryBtn'} isdisabled={true}/>        
        </div>
        <h4 className='mt40'>Primary Dropdown with Img</h4>
        <div className="dFlex">
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'primaryBtn'} isdisabled={false} img={'../assets/icons/faceIcon.svg'} dropIcon={"../assets/icons/chevron-down.svg"} />
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'primaryBtn'} isdisabled={true} img={'../assets/icons/faceIcon.svg'} dropIcon={"../assets/icons/chevron-down.svg"} />
        </div>
        <h4 className='mt40'>Primary Small</h4>
        <div className="dFlex">
        <Button onclick={log} children={'Rest'} classname={'primaryBtnSmall'} isdisabled={false}/>
        <Button onclick={log} children={'Rest Disabled'} classname={'primaryBtnSmall'} isdisabled={true}/>
        </div>
        <h4 className='mt40'>Primary-icon</h4>
        <div className="dFlex">
            <button className="primaryBtnSmall"><img src="../assets/icons/faceIcon.svg" alt="" /> Rest</button>
            <button className="primaryBtnSmall" disabled><img src="../assets/icons/faceIcon.svg" alt="" /> Rest</button>
        </div>
        <h4 className='mt40'>Secondary</h4>
        <div className="dFlex">
        <Button onclick={log} children={'Rest'} classname={'secondaryBtn'} isdisabled={false}/>
        <Button onclick={log} children={'Rest Disabled'} classname={'secondaryBtn'} isdisabled={true}/>
        </div>
        <h4 className='mt40'>secondary Icon</h4>
        <div className="dFlex">
          <ButtonDropdownImg img={'../assets/icons/faceIconBlue.svg'} children={'Rest'} classname={'secondaryBtn'} isdisabled={false} />
          <ButtonDropdownImg img={'../assets/icons/faceIconBlue.svg'} children={'Rest Disabled'} classname={'secondaryBtn'} isdisabled={true} />
        </div>
        <h4 className='mt40'>Secondary Dropdown</h4>
        <div className="dFlex">
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'secondaryBtn'} isdisabled={false}  dropIcon={"../assets/icons/chevron-down-blue.svg"}/>        
          <ButtonDropdownImg onclick={log} children={'Rest Disabled'} classname={'secondaryBtn'} isdisabled={true}  dropIcon={"../assets/icons/chevron-down-blue.svg"}/>        
        </div>
        <h4 className='mt40'>Secondary Dropdown with Img</h4>
        <div className="dFlex">
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'secondaryBtn'} isdisabled={false} img={'../assets/icons/faceIconBlue.svg'} dropIcon={"../assets/icons/chevron-down-blue.svg"}/>
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'secondaryBtn'} isdisabled={true} img={'../assets/icons/faceIconBlue.svg'} dropIcon={"../assets/icons/chevron-down-blue.svg"} />
        </div>
        <h4 className='mt40'>Secondary Small</h4>
        <div className="dFlex">
        <Button onclick={log} children={'Rest'} classname={'secondaryBtnSmall'} isdisabled={false}/>
        <Button onclick={log} children={'Rest Disabled'} classname={'secondaryBtnSmall'} isdisabled={true}/>
        </div>
        <h4 className='mt40'>Secondary Small Img</h4>
        <div className="dFlex">
        <ButtonDropdownImg onclick={log} img={'../assets/icons/faceIconBlue.svg'} children={'Rest'} classname={'secondaryBtnSmall'} isdisabled={false}/>
        <ButtonDropdownImg onclick={log} img={'../assets/icons/faceIconBlue.svg'} children={'Rest Disabled'} classname={'secondaryBtnSmall'} isdisabled={true}/>
        </div>
        <h4 className='mt40'>Dark</h4>
        <div className="dFlex">
        <Button onclick={log} children={'Rest'} classname={'darkBtn'} isdisabled={false}/>
        <Button onclick={log} children={'Rest Disabled'} classname={'darkBtn'} isdisabled={true}/>
        </div>
        
        <h4 className='mt40'>Dark-icon</h4>
        <div className="dFlex">
          <ButtonDropdownImg img={'../assets/icons/faceIcon.svg'} children={'Rest'} classname={'darkBtn'} isdisabled={false} />
          <ButtonDropdownImg img={'../assets/icons/faceIcon.svg'} children={'Rest Disabled'} classname={'darkBtn'} isdisabled={true} />
        </div>
        <h4 className='mt40'>Dark Dropdown</h4>
        <div className="dFlex">
          <PrimaryBtnDropdown onclick={log} children={'Rest'} classname={'darkBtn'} isdisabled={false}/>        
          <PrimaryBtnDropdown onclick={log} children={'Rest Disabled'} classname={'darkBtn'} isdisabled={true}/>        
        </div>
        <h4 className='mt40'>Dark Dropdown with Img</h4>
        <div className="dFlex">
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'darkBtn'} isdisabled={false} img={'../assets/icons/faceIcon.svg'} dropIcon={"../assets/icons/chevron-down.svg"} />
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'darkBtn'} isdisabled={true} img={'../assets/icons/faceIcon.svg'} dropIcon={"../assets/icons/chevron-down.svg"} />
        </div>
        <h4 className='mt40'>Dark Small</h4>
        <div className="dFlex">
        <Button onclick={log} children={'Rest'} classname={'darkBtnSmall'} isdisabled={false}/>
        <Button onclick={log} children={'Rest Disabled'} classname={'darkBtnSmall'} isdisabled={true}/>
        </div>
        <h4 className='mt40'>Success</h4>
        <div className="dFlex">
        <Button onclick={log} children={'Rest'} classname={'successBtn'} isdisabled={false}/>
        <Button onclick={log} children={'Rest Disabled'} classname={'successBtn'} isdisabled={true}/>
        </div>
        <h4 className='mt40'>Success-icon</h4>
        <div className="dFlex">
          <ButtonDropdownImg img={'../assets/icons/faceIcon.svg'} children={'Rest'} classname={'successBtn'} isdisabled={false} />
          <ButtonDropdownImg img={'../assets/icons/faceIcon.svg'} children={'Rest Disabled'} classname={'successBtn'} isdisabled={true} />
        </div>
        <h4 className='mt40'>Success Dropdown</h4>
        <div className="dFlex">
          <PrimaryBtnDropdown onclick={log} children={'Rest'} classname={'successBtn'} isdisabled={false}/>        
          <PrimaryBtnDropdown onclick={log} children={'Rest Disabled'} classname={'successBtn'} isdisabled={true}/>        
        </div>
        <h4 className='mt40'>Primary Dropdown with Img</h4>
        <div className="dFlex">
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'successBtn'} isdisabled={false} img={'../assets/icons/faceIcon.svg'} dropIcon={"../assets/icons/chevron-down.svg"} />
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'successBtn'} isdisabled={true} img={'../assets/icons/faceIcon.svg'} dropIcon={"../assets/icons/chevron-down.svg"} />
        </div>
        <h4 className='mt40'>Secondary</h4>
        <div className="dFlex">
        <Button onclick={log} children={'Rest'} classname={'successBtn'} isdisabled={false}/>
        <Button onclick={log} children={'Rest Disabled'} classname={'successBtn'} isdisabled={true}/>
        </div>
        <h4 className='mt40'>secondary Icon</h4>
        <div className="dFlex">
          <ButtonDropdownImg img={'../assets/icons/faceIconBlue.svg'} children={'Rest'} classname={'successBtn'} isdisabled={false} />
          <ButtonDropdownImg img={'../assets/icons/faceIconBlue.svg'} children={'Rest Disabled'} classname={'successBtn'} isdisabled={true} />
        </div>
        <h4 className='mt40'>Secondary Dropdown</h4>
        <div className="dFlex">
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'successBtn'} isdisabled={false}  dropIcon={"../assets/icons/chevron-down-blue.svg"}/>        
          <ButtonDropdownImg onclick={log} children={'Rest Disabled'} classname={'successBtn'} isdisabled={true}  dropIcon={"../assets/icons/chevron-down-blue.svg"}/>        
        </div>
        <h4 className='mt40'>Secondary Dropdown with Img</h4>
        <div className="dFlex">
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'successBtn'} isdisabled={false} img={'../assets/icons/faceIconBlue.svg'} dropIcon={"../assets/icons/chevron-down-blue.svg"}/>
          <ButtonDropdownImg onclick={log} children={'Rest'} classname={'successBtn'} isdisabled={true} img={'../assets/icons/faceIconBlue.svg'} dropIcon={"../assets/icons/chevron-down-blue.svg"} />
        </div>
        <h4 className='mt40'>Secondary Small</h4>
        <div className="dFlex">
        <Button onclick={log} children={'Rest'} classname={'successBtnSmall'} isdisabled={false}/>
        <Button onclick={log} children={'Rest Disabled'} classname={'successBtnSmall'} isdisabled={true}/>
        </div>
        <h4 className='mt40'>Secondary Small Img</h4>
        <div className="dFlex">
        <ButtonDropdownImg onclick={log} img={'../assets/icons/faceIconBlue.svg'} children={'Rest'} classname={'successBtnSmall'} isdisabled={false}/>
        <ButtonDropdownImg onclick={log} img={'../assets/icons/faceIconBlue.svg'} children={'Rest Disabled'} classname={'successBtnSmall'} isdisabled={true}/>
        </div>
    </div>
  )
}

export default Buttons