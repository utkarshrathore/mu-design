import React, { useState } from 'react'

const PrimaryBtnDropdown = ({onclick,isdisabled,children,classname}) => {
    const [active,setActive] = useState(false)
    const toggleMenu = ()=>{
        setActive(!active)
    }
  return (
    <button className={classname} onClick={()=>{onclick();toggleMenu()}} disabled={isdisabled} onBlur={()=>setActive(false)}>
        {children} <img src="../assets/icons/chevron-down.svg" alt="" />
        <ul className={`btnDropdown ${active? 'active' : ''}`}>
              <li className="btnDropDownItem"><img src="../assets/icons/plus.svg" alt="" /> Add Applicants</li>
              <li className="btnDropDownItem"><img src="../assets/icons/plus.svg" alt="" /> Add Applicants</li>
              <li className="btnDropDownItem"><img src="../assets/icons/plus.svg" alt="" /> Add Applicants</li>
            </ul>
    </button>
  )
}

export default PrimaryBtnDropdown 