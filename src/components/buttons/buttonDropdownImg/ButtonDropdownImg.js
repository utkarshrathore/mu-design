import React, { useState } from 'react'

const ButtonDropdownImg = ({onclick,isdisabled,children,img,classname,dropIcon}) => {
    const [active,setActive] = useState(false)
    const toggleMenu = ()=>{
        setActive(!active)
    }
  return (
    <button className={classname} onClick={()=>{onclick();toggleMenu()}} onBlur={()=>setActive(false)} disabled={isdisabled}>
        <img src={img} alt=""  />
        {children} <img src={dropIcon} alt="" />
        <ul className={`btnDropdown ${active? 'active' : ''}`}>
              <li className="btnDropDownItem"><img src="../assets/icons/plus.svg" alt="" /> Add Applicants</li>
              <li className="btnDropDownItem"><img src="../assets/icons/plus.svg" alt="" /> Add Applicants</li>
              <li className="btnDropDownItem"><img src="../assets/icons/plus.svg" alt="" /> Add Applicants</li>
            </ul>
    </button>
  )
}

export default ButtonDropdownImg