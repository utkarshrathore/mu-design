import React from 'react'

const PrimaryButton = ({onclick,isdisabled,children,classname}) => {
  return (
    <button className={classname} onClick={onclick} disabled={isdisabled}>{children}</button>
  )
}

export default PrimaryButton;