import React from 'react'
import "./CheckboxStyle.css";
import "./../dropdown/DropdownStyle.css";

const Checkbox = () => {
    return (
        <div className="container">
            <div className="dFlex">
                <div class="checkboxGrp blackcheckboxGrp">
                <input type="checkbox" id="Option 1" />
                    <label for="Option 1">
                        Option 1
                    </label>
                </div>

                <div class="checkboxGrp blackcheckboxGrp">
                <input type="checkbox" id="Option 2" checked="yes" disabled="yes"/>
                    <label for="Option 2">
                        Option 2
                    </label>
                </div>

                <div class="checkboxGrp blackcheckboxGrp">
                <input type="checkbox" id="Option 3" disabled="yes"/>
                    <label for="Option 3">
                        Option 3
                    </label>
                </div>

            </div>
        </div>
    )
}

export default Checkbox