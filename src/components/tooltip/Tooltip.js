import React from "react";
import PrimaryButton from "../buttons/primaryButton/PrimaryButton";
import "./tooltipStyle.css";

const Tooltip = ({title,children,tooltipText}) => {
  return (
   <div className="container">
    <div className="dFlex">
    <div className="tooltipOuter">
      <p className="tooltipContent">
       {children}
      </p>
      <div className="tooltipContainer">
        <div className="tooltipHeader">
          <p className="tooltipTitle">{title}</p>
        </div>
        <div className="tooltipBody">
          <p className="text">
            {tooltipText}
          </p>
        </div>
        <div className="tooltipFooter">
            <PrimaryButton classname={'secondaryBtn'} children={'Cancel'}/>
            <PrimaryButton classname={'primaryBtn'} children={'Approve'}/>
        </div>
      </div>
    </div>
    </div>
   </div>
  );
};

export default Tooltip;
